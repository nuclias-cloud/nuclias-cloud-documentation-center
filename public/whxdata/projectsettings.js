// Publish project specific data
(function() {
rh = window.rh;
model = rh.model;
var defaultTopic = "Getting-Started-with-Nuclias/Getting-Started.htm";
rh._.exports(defaultTopic);
rh.consts('DEFAULT_TOPIC', encodeURI("Getting-Started-with-Nuclias/Getting-Started.htm"));
rh.consts('HOME_FILEPATH', encodeURI('index.html'));
rh.consts('START_FILEPATH', encodeURI('index.html'));
rh.consts('HELP_ID', '5897a974-45ec-4696-9e7e-bff779d4546f' || 'preview');
rh.consts('LNG_SUBSTR_SEARCH', 0);

model.publish(rh.consts('KEY_LNG_NAME'), "en");
model.publish(rh.consts('KEY_DIR'), "ltr");
model.publish(rh.consts('KEY_LNG'), {"BreadcrumbStart":"Home: ","BrsNextButton":"Next","BrsPrevButton":"Previous","CloseFavorites":"Close Favorites","ContentsTab":"Contents","CookiesAcceptText":"We ask you to accept cookies for performance, readability and experience purposes. Cookies are used for bookmarking favorite topics and to restore the table of contents, index and glossary on topic change. This setting is asked only once and can be reverted by clearing the browser cookies.","CookiesAcceptButton":"Accept","CookiesDenyButton":"Later","EditFavorites":"Edit Favorites","FavoriteArticle":"saved article","FavoriteArticles":"saved articles","FullScreenButton":"Full Screen","GlossaryTab":"Glossary","GlossResultHeaderLabel":"Glossary Dictionary","HideLeftPanelTip":"Hide Left Panel","HideResults":"Hide Results","HomeButton":"Nuclias Cloud Documentation","HomePageLogoTitle":"Nuclias Cloud Documentation","HomePageSubtitle":"","IndexTab":"Index","MiniTOCCaption":"In this Topic","NoResultsFoundText":"No Results Found","PrintButtonTip":"Print","RemoveFavItem":"Remove ","RemoveHighlight":"Remove Highlight","ResultsFoundText":"%1 result(s) found for %2","SearchPlaceHolder":"Search...","SetAsFavorite":"Set as Favorite","ShowLeftPanelTip":"Show Left Panel","TOCTileArticlesCount":"article(s)","ToTopButtonTip":"Go to Top","UnsetAsFavorite":"Unset as Favorite","TopicHiddenText":"This topic is filtered out by the selected filters.","ResetFilters":"Reset Filters"});

model.publish(rh.consts('KEY_HEADER_TITLE'), "Nuclias Cloud Documentation");
model.publish(rh.consts('PDF_FILE_NAME'), "DBA-1510P_A1_Datasheet_2.6(WW).pdf");
model.publish(rh.consts('MAX_SEARCH_RESULTS'), "");
model.publish(rh.consts('KEY_SKIN_FOLDER_NAME'), "Nuclias-Cloud");
model.publish(rh.consts('KEY_SUBSTR_SEARCH'), "");
})();
