window.onload = function(){
    function langSwitch(lang,dest){
        switch (lang){
        case 'en': $lang = 'en/'; break;
        case 'zh': $lang = 'zh/'; break;
        case 'sc': $lang = 'sc/'; break;
        default: break;
        }
        switch (dest){
        case 'en': $dest = 'en/'; break;
        case 'zh': $dest = 'zh/'; break;
        case 'sc': $dest = 'sc/'; break;
        default: break;
        }
        document.querySelector('.langSwitch-box').style.display="none"
        $destChf = (top.location.href).replace($lang,$dest);
        top.location.href = $destChf;
    }

    function TOCsTabclick(){
        $("#rh-leftpanel").on("click", ".RH-LAYOUT-LEFTPANEL-item-text", function(){

            if($(this).attr('href') != undefined){
                var url = $(this).attr('href')
                if(url.search('#') != -1){
                    var urlsplit = url.split('#')
                    var urlHash = '#' + urlsplit[1]
    
                    window.location=urlHash
                    window.location.reload();
                }

            }
            
        });
    }
    
    if( $(window).width() <= 1000 ){
    
        $('.RH-LAYOUT-RIGHTPANEL-container').addClass("frameless-hide");
        $('.RH-LAYOUT-LEFTPANEL-container').addClass("frameless-hide");
        $('.RH-LAYOUT-SEARCHRESULTS').addClass("frameless-hide");

        TOCsTabclick();
        
    } 

    $(window).resize(function() {
       if($(window).width() <= 1000){
        $('.rh-BODY-wrapper').removeClass('rh-layout-left-panel-expanded')
        $('#rh-searchresults').addClass("frameless-hide");
        TOCsTabclick();

       } else {
            $('.RH-LAYOUT-SEARCHRESULTS').removeClass("frameless-hide");
       }
    }); 
}


